import Vue from 'vue'
//in webpack environment:
import vuejsStorage from 'vuejs-storage'
//in browser script tag:
Vue.use(vuejsStorage)
