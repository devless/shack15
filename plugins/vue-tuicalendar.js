import Vue from 'vue'
import VueTuicalendar from '@lkmadushan/vue-tuicalendar'
// You need a specific loader for CSS files like https://github.com/webpack/css-loader
import 'tui-calendar/dist/tui-calendar.min.css'

Vue.use(VueTuicalendar)
