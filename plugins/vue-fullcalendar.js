import Vue from 'vue'
import FullCalendar from 'vue-full-calendar'
import 'fullcalendar-scheduler'

Vue.use(FullCalendar)
