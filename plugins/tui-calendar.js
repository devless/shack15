import Vue from 'vue'
import 'tui-calendar/dist/tui-calendar.css'
import { Calendar } from '@toast-ui/vue-calendar'

Vue.component('calendar', Calendar)
