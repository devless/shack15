export default {
  SET_SPACE_DATA: (state, spaces) => {
    state.spaces = spaces
  },
  SET_CURRENT_SPACE: (state, space) => {
    state.base_url = `/space/${space.tag}`
    state.current_space = space
  },
  CHANGE_LOADING: (state, opt = null) => {
    if (opt !== null) {
      state.loading = opt
    } else {
      state.loading = !state.loading
    }
  },
  SET_SPECIFIC_USER: (state, user) => (state.user = user),
  SET_ANY_DATA: (state, data) => (state.anyData = data)
}
