export default {
  getCurrency(state) {
    if (state.current_space.currency === 'usd') {
      return '$'
    }
    return '£'
  },
  getCurrentSpaceId(state) {
    return state.current_space.id
  }
}
