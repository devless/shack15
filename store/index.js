import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import state from './state'
import mutations from './mutations'
import auth from './modules/auth'
import events from './modules/events'
import members from './modules/members'
import plans from './modules/plans'
import rooms from './modules/rooms'
import tables from './modules/tables'
import tags from './modules/tags'
import wellness from './modules/wellness'
import givebackoptions from './modules/givebackoptions'
import inventory from './modules/inventory'
import bookings from './modules/bookings'
import companies from './modules/companies'
import dashboard from './modules/dashboard'
import admins from './modules/admins'
import checkins from './modules/checkins'
import calendar from './modules/calendar'

const createStore = () => {
  return new Vuex.Store({
    modules: {
      auth,
      events,
      members,
      plans,
      rooms,
      wellness,
      tables,
      tags,
      admins,
      givebackoptions,
      inventory,
      bookings,
      companies,
      dashboard,
      checkins,
      calendar
    },
    state,
    actions,
    mutations,
    getters
  })
}

export default createStore
