const actions = {
  async nuxtServerInit(
    { state, commit, dispatch },
    { app, req, res, redirect }
  ) {
    let path = req.path.split('/')
    let token = app.$auth.$storage.getUniversal('token', false)

    if (!token && req.path !== '/forgot-password/reset') {
      redirect('/login')
    } else if (req.path !== '/forgot-password/reset') {
      app.$devless.setToken(token)

      await this.$devless.call('core', 'getUserProfile').then(async resp => {
        if (resp.status_code === 1000) {
          dispatch('getSpaces')
          if (path.length > 2) {
            await dispatch('getSpaceByTag', path[2])
          }
          commit('SET_USER', {
            profile: resp.payload.results,
            token: token
          })
          commit('CHANGE_LOADING', false)
          commit('CHANGE_AUTH_STATE', true)
        }
      })
    }
  },
  async getSpaces({ commit }) {
    try {
      await this.$devless.queryData('core', 'workspace').then(res => {
        if (res.status_code === 625) {
          commit('SET_SPACE_DATA', res.payload.results)
          return
        }
        this.$toast.error(
          'Critical Error Retrieving Space Details! Contact development team.'
        )
      })
    } catch (e) {
      this.$sentry.captureException(e)
    }
  },
  async getSpaceByTag({ commit }, tag) {
    try {
      commit('CHANGE_LOADING', true)
      await this.$devless
        .queryData('core', 'workspace', {
          where: `tag,${tag}`
        })
        .then(res => {
          commit('CHANGE_LOADING', false)
          if (res.status_code === 625) {
            commit('SET_CURRENT_SPACE', res.payload.results[0])
            return
          }
          this.$toast.error(
            'Critical Error Retrieving Space Details! Contact development team.'
          )
        })
    } catch (e) {
      this.$sentry.captureException(e)
    }
  },
  async uploadImage({ commit }, image) {
    try {
      return await this.$devless
        .call('aws', 'uploadBase64File', [image])
        .then(res => {
          if (res.status_code === 1000) {
            return res.payload
          }
          this.$toast.error(res.message)
        })
    } catch (e) {
      this.$sentry.captureException(e)
    }
  },
  async getSpecificUser({ commit }, user_id) {
    try {
      commit('CHANGE_LOADING')
      await this.$devless
        .call('core', 'getAUserProfile', [
          {
            user_id: user_id
          }
        ])
        .then(res => {
          commit('CHANGE_LOADING')
          if (res.status_code === 1000) {
            commit('SET_SPECIFIC_USER', res.payload.results[0])
            return res.payload.results[0]
          }
          this.$toast.error('Error Retrieving User Details. Try Again ....')
        })
    } catch (e) {
      this.$sentry.captureException(e)
    }
  },
  async superApi({ commit }, payload) {
    try {
      return await this.$devless
        .call('core', 'superAPI', [payload])
        .then(res => {
          if (res.status_code === 1000) {
            if (payload.commit) {
              commit(payload.commit, res.payload.results)
            }
            if (payload.success) {
              this.$toast.success(payload.success)
            }
            return res
          }
          this.$toast.error(payload.error)
        })
    } catch (e) {
      console.log('&&&&&&&&&&&&&&&&&&')
      console.log(e)
      console.log('&&&&&&&&&&&&&&&&&&')
      this.$sentry.captureException(e)
    }
  }
}

export default actions
