export default {
  base_url: undefined,
  spaces: [],
  current_space: {},
  loading: false,
  user: {
    id: '',
    first_name: '',
    last_name: ''
  }
}
