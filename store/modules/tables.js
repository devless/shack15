export default {
  state: {
    data: [],
    table: undefined,
    openings: undefined,
    reservations: []
  },
  mutations: {
    SET_DATA: (state, payload) => {
      state.data = payload
    },
    SET_TABLE: (state, table) => {
      state.table = table
    },
    ADD_TABLE: (state, payload) => {
      state.data.push(payload)
    },
    UPDATE_TABLE: (state, payload) => {
      state.data[payload.index] = payload.row
    },
    DELETE_TABLE: (state, index) => {
      state.data.splice(index, 1)
    },
    SET_OPENING_DAYS: (state, data) => {
      state.openings = data
    },
    SET_RESERVATIONS: (state, reservations) => {
      state.reservations = reservations
    }
  },
  actions: {
    async fetchTables({ commit }, workspace_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'restaurant', {
            where: `core_workspace_id,${workspace_id}`
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_DATA', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Failed to Retrieve Tables. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getSingleTable({ commit, getters }, table_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'restaurant', {
            where: [
              `core_workspace_id,${getters.getCurrentSpaceId}`,
              `id,${table_id}`
            ]
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625 && res.payload.results.length > 0) {
              res.payload.results[0]['images'] = JSON.parse(
                res.payload.results[0]['images']
              )
              commit('SET_TABLE', { row: res.payload.results[0] })
              return res.payload.results[0]
            }
          })
      } catch (e) {
        this.$toast.error('Failed to Retrieve Tables. Try Again ...')
        this.$sentry.captureException(e)
      }
    },
    async getTableOpeningDays({ commit }, room_id) {
      try {
        return await this.$devless
          .queryData('core', 'restaurant_opening_days', {
            where: 'core_restaurant_id,' + room_id
          })
          .then(res => {
            if (res.status_code === 625) {
              // commit('SET_OPENING_DAYS', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Failed to Retrieve Room Openings. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addTable({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .addData('core', 'restaurant', payload)
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_TABLE', payload)
              // this.$router.go(-1)
              this.$toast.success('Table Added Successfully')
              return res.payload.entry_id
            }
            this.$toast.error('Table Addition Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateTable({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .updateData('core', 'restaurant', 'id', payload.row.id, payload.row)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 619) {
              commit('UPDATE_TABLE', payload)
              this.$router.go(-1)
              this.$toast.success('Table Updated Successfully')
              return
            }
            this.$toast.error('Table Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async deleteTable({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('core', 'restaurant', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('DELETE_TABLE', payload.index)
              this.$toast.success('Table Deleted Successfully')
              return
            }
            this.$toast.error('Table Delete Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async fetchReservations({ commit, getters }, date) {
      try {
        commit('CHANGE_LOADING', true)

        const moment = require('moment')

        return await this.$devless
          .queryData('core', 'restaurant_reservations', {
            where: [
              `core_workspace_id,${getters.getCurrentSpaceId}`,
              `date,${date}`
            ],
            related: '*'
          })
          .then(res => {
            if (res.status_code === 625) {
              commit('CHANGE_LOADING', false)
              let reservations = _.map(res.payload.results, o => {
                o.member_id = o.related._devless_users[0].id
                o.member = `${o.related._devless_users[0].first_name} ${
                  o.related._devless_users[0].last_name
                }`
                o.created_on = moment.unix(o.created_on).format('DD/MM/YYYY')
                o.table = o.related.restaurant[0].name
                return o
              })

              commit('SET_RESERVATIONS', reservations)
              return res.payload.results
            }

            this.$toast.error('Error Fetching Reservations')
          })
      } catch (e) {
        console.log(e)
        this.$sentry.captureException(e)
      }
    }
  }
}
