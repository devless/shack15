export default {
  state: {
    data: [],
    users: []
  },
  mutations: {
    SET_DATA: (state, data) => {
      state.data = data
    },
    SET_COMPANY_USERS: (state, users) => {
      state.users = users
    },
    ADD_COMPANY: (state, payload) => {
      state.data.push(payload)
    },
    REMOVE_COMPANY: (state, index) => {
      state.companies.splice(index, 1)
    }
  },
  actions: {
    async getCompanies({ commit, dispatch }, workspace_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await dispatch('superApi', {
          table: 'devless_companies',
          action: 'read',
          queries: [{ where: ['workspace_id', workspace_id] }]
        }).then(res => {
          commit('CHANGE_LOADING', false)
          if (res.status_code === 1000) {
            commit('SET_DATA', res.payload)
            return res
          }
          this.$toast.error('Error Retrieving Companies. Try Again ...')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getUsersByCompany({ commit, dispatch }, company_id) {
      try {
        await dispatch('superApi', {
          table: 'users',
          action: 'read',
          queries: [{ where: ['devless_companies_id', company_id] }]
        }).then(res => {
          if (res.status_code === 1000) {
            commit('SET_COMPANY_USERS', res.payload)
            return res
          }
          this.$toast.error(
            'Error Retrieving Users Belonging To These Company. Try Again ...'
          )
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addCompanies({ commit, dispatch }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await dispatch('superApi', {
          table: 'devless_companies',
          action: 'create',
          queries: [],
          records: payload
        }).then(res => {
          if (res.status_code === 1000) {
            this.$toast.success('Company Added Successfully')
            this.$router.go(-1)
            return
          }
          this.$toast.error('Error Adding Company')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    removeCompany({ state, commit, dispatch }, payload) {
      dispatch('superApi', {
        table: 'devless_companies',
        action: 'destroy',
        queries: [
          {
            where: ['id', payload.row.id]
          }
        ]
      }).then(res => {
        if (res.status_code == 1000) {
          commit('REMOVE_COMPANY', payload.index)
          this.$toast.success('Record Removed Successfully.')
          return
        }
        this.$toast.error('Error Removing Record. Try Again...')
      })
    }
  }
}
