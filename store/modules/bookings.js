import moment from 'moment'

export default {
  state: {
    data: [],
    resources: {}
  },
  mutations: {
    SET_BOOKINGS: (state, bookings) => {
      state.data = bookings
    },
    SET_RESOURCES: (state, resources) => {
      state.resources = resources
    },
    SET_RESOURCE_DATA: (state, data) => {
      data.map(v => {
        state[v.key] = v.value
      })
    }
  },
  actions: {
    async getAllBookings({ commit, dispatch }, payload) {
      try {
        return await this.$devless
          .call('core', 'getAllBookings', [payload])
          .then(res => {
            if (res.status_code === 1000) {
              commit('SET_RESOURCES', res.payload.results)
              return res.payload.results
            }

            this.$toast.error('Error Retrieving Bookings. Try Again')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getAllResources({ commit, dispatch }, workspace_id) {
      try {
        return await Promise.all([
          dispatch('fetchTables', workspace_id),
          dispatch('fetchRooms', workspace_id),
          dispatch('fetchData', workspace_id)
        ]).then(([tables, rooms, wellness]) => {
          commit('SET_RESOURCE_DATA', [
            {
              key: 'tables',
              value: tables
            },
            {
              key: 'rooms',
              value: rooms
            },
            {
              key: 'wellness',
              value: wellness
            }
          ])

          const _ = require('lodash')
          const randomColor = require('randomcolor')

          wellness = _.map(wellness, v => {
            v.name = v.title
            return v
          })

          function format(v) {
            let color = randomColor({
              luminosity: 'dark'
            })

            try {
              v.title = v.name
            } catch (e) {
              this.$sentry.captureException(e)
            }

            v.dt = v.id
            v.id = _.replace(v.title, ' ', '_')

            v.eventColor = color
            v.eventBackgroundColor = color + '1A'
            v.eventTextColor = color
            v.eventBorderColor = color

            return v
          }

          let resources = _.map(_.concat(tables, rooms, wellness), format)

          return resources
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  },
  getters: {
    setTables: state => {
      let tables = []

      _.map(state.tables, v => {
        tables = _.map(state.resources.restaurantReservations, d => {
          if (v.dt == d.core_restaurant_id) {
            d.resourceId = _.replace(v.name, ' ', '_')
            d.title = `Table booked for #${d.seats}`
            d.raw = v
            d.type = 'tables'
            d.color = v.eventTextColor
            d.backgroundColor = v.eventBackgroundColor
            d.start = moment(`${d.date} ${d.from}`, 'DD/MM/YYYY HH:mm').format(
              'YYYY-MM-DD HH:mm'
            )
            d.end = moment(`${d.date} ${d.to}`, 'DD/MM/YYYY HH:mm').format(
              'YYYY-MM-DD HH:mm'
            )

            return d
          }
        })
      })

      return tables
    },
    setRooms: state => {
      let rooms = []
      _.map(state.rooms, v => {
        _.map(state.resources.roomBookings, d => {
          if (v.dt == d.core_rooms_id) {
            d.resourceId = _.replace(v.name, ' ', '_')
            d.title = d.purpose
            d.type = 'rooms'
            d.raw = v
            d.start = `${moment(`${d.when}`, 'DD/MM/YYYY').format(
              'YYYY-MM-DD'
            )} ${d.from}`
            d.end = moment(`${d.when} ${d.to}`, 'DD/MM/YYYY HH:mm').format(
              'YYYY-MM-DD HH:mm'
            )

            rooms.push(d)
          }
        })
      })

      return rooms
    },
    setWellness: state => {
      let wellness = []

      if (state.wellness) {
        state.wellness.map(v => {
          let slots = JSON.parse(v.slots)
          slots.map(data => {
            data.title = `Session Slot - ${slots.length}`
            data.originalTitle = v.title
            data.start = moment(
              `${data.date} ${data.time}`,
              'DD/MM/YYYY hh:mm'
            ).format('YYYY-MM-DD hh:mm')
            data.id = v.dt
            data.raw = v
            data.type = 'wellness'
            data.resourceId = _.replace(v.title, ' ', '_')

            wellness.push(data)
          })
        })

        return wellness
      }
    }
  }
}
