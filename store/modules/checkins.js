const checkins = {
  state: {
    count: []
  },
  mutations: {
    addToCount(state, payload) {
      state.count.push(payload)
    },
    setCount(state, payload) {
      state.count = payload
    }
  }
}

export default checkins
