export default {
  state: {
    data: []
  },
  mutations: {
    SET_ADMINS: (state, data) => {
      state.data = data
    }
  },
  actions: {
    async getAllAdmins({ commit, dispatch }) {
      await dispatch('superApi', {
        table: 'users',
        action: 'read',
        queries: [
          {
            join: [
              'devless_admin_role',
              'users.id',
              '=',
              'devless_admin_role.users_id'
            ]
          }
        ]
      }).then(res => {
        if (res.status_code == 1000) {
          commit('SET_ADMINS', res.payload)
        }
      })
    }
  }
}
