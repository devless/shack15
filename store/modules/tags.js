export default {
  state: {
    data: [],
    current_tag: {}
  },
  mutations: {
    SET_TAGS: (state, data) => {
      state.data = data
    },
    ADD_TAG: (state, tag) => {
      state.data.push(tag)
    },
    UPDATE_TAG: (state, payload) => {
      state.data[payload.index] = payload.row
    },
    REMOVE_TAG: (state, index) => {
      state.data.splice(index, 1)
    },
    SET_CURRENT_TAG: (state, tag) => {
      state.current_tag = tag
    }
  },
  actions: {
    async getTags({ commit }, workspace_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('devless', 'tags', {
            where: `workspace,${workspace_id}`
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_TAGS', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Error Retrieving Tags. Try Again...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addTag({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless.addData('devless', 'tags', payload).then(res => {
          commit('CHANGE_LOADING', false)
          if (res.status_code === 609) {
            payload.id = res.payload.entry_id
            commit('ADD_TAG', payload)
            this.$router.go(-1)
            this.$toast.success('Tag Added Successfully')
            return
          }
          this.$toast.error('Tag Addition Failed')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateTag({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .updateData('devless', 'tags', 'id', payload.row.id, payload.row)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 619) {
              commit('UPDATE_TAG', payload)
              this.$router.go(-1)
              this.$toast.success('Tag Updated Successfully')
              return
            }
            this.$toast.error('Tag Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async removeTag({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('devless', 'tags', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('REMOVE_TAG', payload.index)
              this.$toast.success('Tag Removed Successfully')
              return
            }
            this.$toast.error('Tag Removal Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
