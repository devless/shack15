import moment from 'moment'

export default {
  state: {
    loading: true,
    dashObj: {},
    offset: 0,
    size: 10,
    bookings: []
  },
  mutations: {
    SET_OBJ: (state, obj) => {
      state.dashObj = obj
    },
    SET_DASH_LOADING: (state, opt = null) => {
      if (opt !== null) {
        state.loading = opt
      } else {
        state.loading = !state.loading
      }
    },
    SET_OFFSET: (state, offset) => {
      state.offset = offset
    },
    SET_LATEST_BOOKINGS: (state, payload) => {
      state.bookings = payload
    }
  },
  actions: {
    async getDataObj({ state, commit, dispatch, getters }, payload) {
      try {
        const moment = require('moment')

        Promise.all([
          dispatch('getAllMembers', payload.workspace_id),
          dispatch('getAllBookings', {
            workspace_id: getters.getCurrentSpaceId,
            date: moment().format('DD/MM/YYYY')
          }),
          dispatch('getLatestOrders', {
            workspace_id: getters.getCurrentSpaceId
          }),
          dispatch('fetchLatestEvents', {
            workspace_id: getters.getCurrentSpaceId,
            start_date: moment().format('DD/MM/YYYY')
          }),
          dispatch('getActivities'),
          dispatch('getLatestBookings')
        ])
          .then(([members, bookings, inventory, events, activities]) => {
            let bookingsCount = 0
            _.map(bookings, v => {
              bookingsCount = bookingsCount + v.length
            })

            let membersCount = 0
            _.each(members, o => {
              if (
                o.last_checkin_date &&
                o.check_in &&
                moment(o.last_checkin_date * 1000).format('YYYY-MM-DD') ==
                  moment().format('YYYY-MM-DD')
              ) {
                membersCount += 1
              }
            })
            let inventoryCount = inventory.length
            let eventsCount = events.properties.current_count

            commit('SET_OBJ', {
              membersCount,
              bookingsCount,
              inventoryCount,
              eventsCount,
              activities
            })
          })
          .finally(() => {
            commit('SET_DASH_LOADING', false)
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getActivities({ state, commit, getters }) {
      try {
        return await this.$devless
          .queryData('core', 'activity_log', {
            where: [`core_workspace_id, ${getters.getCurrentSpaceId}`],
            desc: 'id',
            size: state.size,
            related: '*',
            offset: state.offset
          })
          .then(res => {
            if (res.status_code === 625) {
              return res.payload
            }
            thisl.$toast.error('Error Retrieving Activities. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getLatestBookings({ commit, getters }) {
      try {
        const moment = require('moment')
        return await this.$devless
          .queryData('core', 'room_bookings', {
            where: [
              `when,${moment().format('DD/MM/YYYY')}`,
              `core_workspace_id,${getters.getCurrentSpaceId}`
            ],
            size: 10,
            related: '*'
          })
          .then(res => {
            if (res.status_code === 625) {
              commit('SET_LATEST_BOOKINGS', res.payload.results)
              return res
            }
            this.$toast.error('Error Retrieving Latest Bookings. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
