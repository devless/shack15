const events = {
  state: {
    events: [],
    categories: [],
    schedule: undefined,
    attendees: []
  },
  mutations: {
    SET_EVENTS: (state, events) => {
      state.events = events
    },
    ADD_EVENT: (state, event) => {
      state.events.push(event)
    },
    UPDATE_EVENT: (state, event) => {
      state.events[event.index] = event.data
    },
    DELETE_EVENT: (state, event) => {
      let index = _.findIndex(state.events, o => {
        return o.id === event.id
      })
      state.events.splice(index, 1)
    },
    SET_CATEGORY: (state, categories) => {
      state.categories = categories
    },
    ADD_ROOM_CATEGORY: (state, category) => {
      state.categories.push(category)
    },
    UPDATE_CATEGORY: (state, category) => {
      state.categories[category.index] = category.data
    },
    DELETE_CATEGORY: (state, index) => {
      state.categories.splice(index, 1)
    },
    SET_SCHEDULE: (state, schedule) => {
      state.schedule = schedule
    },
    SET_ATTENDEES: (state, payload) => {
      state.attendees = payload
    },
    ADD_ATTENDEE: (state, attendee) => {
      state.attendees.push(attendee)
    },
    REMOVE_ATTENDEE: (state, index) => {
      state.attendees.splice(index, 1)
    }
  },
  actions: {
    async fetchEvents({ commit }, workspace_id) {
      try {
        return await this.$devless
          .queryData('core', 'events', {
            where: `core_workspace_id,${workspace_id}`,
            related: '*'
          })
          .then(res => {
            if (res.status_code === 625) {
              commit('SET_EVENTS', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Error Retrieving Events. Try Again ...')
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async fetchLatestEvents({ commit }, payload) {
      try {
        return await this.$devless
          .queryData('core', 'events', {
            where: [
              `core_workspace_id,${payload.workspace_id}`,
              `date,${payload.start_date}`
            ],
            related: '*'
          })
          .then(res => {
            if (res.status_code === 625) {
              return res.payload
            }
            this.$toast.error('Error Retrieving Events. Try Again ...')
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async addEvent({ commit }, payload) {
      try {
        return await this.$devless
          .addData('core', 'events', payload)
          .then(res => {
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_EVENT', payload)
              this.$toast.success('Event added successfully')
              this.$router.go(-1)
              return true
            }
            this.$toast.error('Unable to add event.')
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async editEvent({ commit }, payload) {
      try {
        return await this.$devless
          .updateData('core', 'events', 'id', payload.id, payload)
          .then(res => {
            if (res.status_code === 619) {
              commit('UPDATE_EVENT', payload)
              this.$toast.success('Event updated successfully')
              return true
            }
            this.$toast.error('Unable to edit event')
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async deleteEvent({ commit }, payload) {
      try {
        return await this.$devless
          .deleteData('core', 'events', 'id', payload.id)
          .then(res => {
            if (res.status_code === 636) {
              commit('DELETE_EVENT', payload)
              this.$toast.success('Event removed successfully')
              return true
            }
            this.$toast.error('Unable to delete event')
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async fetchCategories({ commit }) {
      try {
        await this.$devless.queryData('core', 'event_categories').then(res => {
          if (res.status_code === 625) {
            commit('SET_CATEGORY', res.payload.results)
            return
          }
          this.$toast.error(res.message)
        })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async addCategory({ commit }, payload) {
      try {
        return await this.$devless
          .addData('core', 'event_categories', payload)
          .then(res => {
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_ROOM_CATEGORY', payload)
              this.$toast.success('Category Added Successfully')
              return true
            }
            this.$toast.error('Category Addition Failed. Try Again ...')
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    // async updateCategory({ commit }, payload) {
    //   try {
    //     await this.$devless
    //       .updateData('core', 'event_categories', 'id', payload.id, payload)
    //       .then(res => {
    //         if (res.status_code === 636) {
    //           commit('UPDATE_CATEGORY', payload)
    //           this.$toast.success('Category removed successfully')
    //           return true
    //         }
    //         this.$toast.error('Error Deleting Category. Try again ...')
    //         return false
    //       })
    //   } catch (error) {
    //     this.$sentry.captureException(error)
    //   }
    // },
    async deleteCategory({ state, commit }, payload) {
      try {
        await this.$devless
          .deleteData(
            'core',
            'event_categories',
            'id',
            state.categories[payload].id
          )
          .then(res => {
            if (res.status_code === 636) {
              commit('DELETE_CATEGORY', payload)
              this.$toast.success('Category Deleted Successful')
              return
            }
            this.$toast.error('Category Deletion Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    setSchedule({ commit }, payload) {
      commit('SET_SCHEDULE', payload.data)
      this.$router.push(payload.route)
    },
    async getAttendees({ commit }, id) {
      // let token = this.$auth.$storage.getUniversal('token', false)
      // await this.$devless.setToken(token)

      try {
        await this.$devless
          .queryData('core', 'event_attendees', {
            where: [`core_events_id,${id}`],
            desc: 'id'
          })
          .then(res => {
            if (res.status_code === 625) {
              commit('SET_ATTENDEES', res.payload.results)
              return
            }
            this.$toast.error('Attendees Retrieval Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async removeAttendee({ commit }, payload) {
      try {
        this.$devless
          .call('core', 'ticketRefund', [{ ticket_code: payload.row.ticket }])
          .then(res => {
            if (res.status_code === 1000) {
              commit('REMOVE_ATTENDEE', payload.index)
              this.$toast.success(res.message)
              return
            }
            this.$toast.error(res.message)
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getSingleEvent({ dispatch }, id) {
      try {
        return await this.$devless
          .queryData('core', 'events', {
            where: `id,${id}`,
            related: '*'
          })
          .then(res => {
            if (res.status_code === 625) {
              return res.payload.results
            }
            this.$router.go(-1)
            this.$toast.error('Error Retrieving Event. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException('Error retrieving single room ' + e)
      }
    }
  },
  getters: {}
}

export default events
// axios.all([getUserAccount(), getUserPermissions()])
//   .then(axios.spread(function (acct, perms) {
//     // Both requests are now complete
//   }));
