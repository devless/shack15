import randomColor from 'randomcolor'
const moment = require('moment')

export default {
  state: {
    data: [],
    resources: []
  },
  mutations: {
    SET_RESOURCE_DATAA: (state, data) => {
      data.map(v => {
        state[v.key] = v.value
      })
    }
  },
  actions: {
    async getCalendarResources({ commit, dispatch, getters }) {
      try {
        return await Promise.all([
          dispatch('fetchTables', getters.getCurrentSpaceId),
          dispatch('fetchRooms', getters.getCurrentSpaceId)
          // dispatch('fetchData', getters.getCurrentSpaceId)
        ]).then(([tables, rooms]) => {
          rooms = _.map(rooms, o => {
            let color = randomColor({
              luminosity: 'dark'
            })
            //
            return {
              id: _.replace(o.name, ' ', '_'),
              name: o.name,
              type: 'rooms',
              price_per_hour: o.price_per_hour,
              bgColor: (o.bgColor = color + '1A'),
              color: color,
              checked: true,
              borderColor: color
            }
          })

          tables = _.map(tables, o => {
            let color = randomColor({
              luminosity: 'dark'
            })

            return {
              ...o,
              id: _.replace(o.name, ' ', '_'),
              type: 'tables',
              bgColor: (o.bgColor = color + '1A'),
              color: color,
              checked: true,
              borderColor: color
            }
          })

          let others = [
            {
              id: 'wellness_event',
              name: 'Wellness Sessions',
              type: 'wellness',
              bgColor: '#0f9e591A',
              color: '#0f9e59',
              checked: true,
              borderColor: '#0f9e59'
            }
          ]

          commit('SET_RESOURCE_DATAA', [
            {
              key: 'tables',
              value: tables
            },
            {
              key: 'rooms',
              value: rooms
            },
            {
              key: 'others',
              value: others
            }
          ])

          let resources = _.concat(rooms, tables, others)
          commit('SET_RESOURCE_DATAA', [
            {
              key: 'resources',
              value: resources
            }
          ])

          return resources
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    getRoomBookings({ commit, dispatch, getters }, payload) {
      try {
        return dispatch('superApi', {
          table: 'core_rooms',
          action: 'read',
          queries: [
            {
              where: [
                'core_room_bookings.core_workspace_id',
                getters.getCurrentSpaceId
              ]
            },
            {
              join: [
                'core_room_bookings',
                'core_rooms.id',
                '=',
                'core_room_bookings.core_rooms_id'
              ]
            },
            {
              join: ['users', 'core_room_bookings.users_id', '=', 'users.id']
            },
            {
              whereRaw: [
                `STR_TO_DATE(core_room_bookings.when, '%d/%m/%Y') >= Date('${
                  payload.start
                }')`
              ]
            },
            {
              whereRaw: [
                `STR_TO_DATE(core_room_bookings.when, '%d/%m/%Y') <= Date('${
                  payload.end
                }')`
              ]
            },
            {
              select: [
                'core_room_bookings.id as booking_id',
                'core_room_bookings.*',
                'users.*',
                'core_rooms.*'
              ]
            }
          ]
        }).then(res => {
          if (res.status_code == 1000) {
            return res.payload
          }

          return this.$toast.error(
            'Error Retrieving Room Bookings. Try Again ...'
          )
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    getTableReservations({ commit, dispatch, getters }, payload) {
      try {
        return dispatch('superApi', {
          table: 'core_restaurant',
          action: 'read',
          queries: [
            {
              join: [
                'core_restaurant_reservations',
                'core_restaurant_reservations.core_restaurant_id',
                '=',
                'core_restaurant.id'
              ]
            },
            {
              join: [
                'users',
                'core_restaurant_reservations.users_id',
                '=',
                'users.id'
              ]
            },
            {
              where: [
                'core_restaurant_reservations.core_workspace_id',
                getters.getCurrentSpaceId
              ]
            },
            {
              whereRaw: [
                `STR_TO_DATE(core_restaurant_reservations.date, '%d/%m/%Y') >= Date('${
                  payload.start
                }')`
              ]
            },
            {
              whereRaw: [
                `STR_TO_DATE(core_restaurant_reservations.date, '%d/%m/%Y') <= Date('${
                  payload.end
                }')`
              ]
            },
            {
              select: [
                'core_restaurant_reservations.id as booking_id',
                'core_restaurant_reservations.*',
                'users.*',
                'core_restaurant.*'
              ]
            }
          ]
        }).then(res => {
          if (res.status_code == 1000) {
            return res.payload
          }
          return this.$toast.error(
            'Error Retrieving Table Reservations. Try Again ...'
          )
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    getAllWellnessSessions({ commit, dispatch, getters }) {
      try {
        return this.$devless
          .call('core', 'getAllWellnessSessions', [])
          .then(res => {
            if (res.status_code == 1000) {
              return res.payload
            }
            return this.$toast.error(
              'Error Retrieving Wellness Sessions. Try Again...'
            )
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getBookings({ state, commit, dispatch, getters }, payload) {
      try {
        return await Promise.all([
          dispatch('getRoomBookings', payload),
          dispatch('getTableReservations', payload),
          dispatch('getAllWellnessSessions')
        ]).then(([bRooms, bTables, bWellness]) => {
          const { rooms } = state
          const { tables } = state
          const { others } = state

          let roomBookings = _.map(bRooms, o => {
            let room = _.find(rooms, ['name', o.name])
            return {
              id: o.id,
              calendarId: room.id,
              raw: {
                ...o,
                type: room.type
              },
              category: 'time',
              dueDateClass: '',
              title: `${_.upperFirst(o.purpose)}`,
              start: moment(
                `${o.when} ${o.from}${payload.timezone ==
                  'America/Los_Angeles' && '-07:00'}`,
                'DD/MM/YYYY HH:mmZ'
              ).format(),
              end: moment(
                `${o.when} ${o.to}${payload.timezone == 'America/Los_Angeles' &&
                  '-07:00'}`,
                'DD/MM/YYYY HH:mmZ'
              ).format()
            }
          })

          let reservations = _.map(bTables, o => {
            let table = _.find(tables, ['name', o.name])

            return {
              ...o,
              calendarId: table.id,
              raw: {
                ...o,
                type: table.type
              },
              type: table.type,
              category: 'time',
              dueDateClass: '',
              title: `${o.seats} seat(s) reserved by ${o.first_name}`,
              start: moment(
                `${o.date} ${o.from}${payload.timezone ==
                  'America/Los_Angeles' && '-07:00'}`,
                'DD/MM/YYYY HH:mmZ'
              ).format(),
              end: moment(
                `${o.date} ${o.to}${payload.timezone == 'America/Los_Angeles' &&
                  '-07:00'}`,
                'DD/MM/YYYY HH:mmZ'
              ).format()
            }
          })

          let wellness = _.find(others, ['type', 'wellness'])
          let sessions = []

          _.each(bWellness.results, o => {
            if (o.slots) {
              _.each(o.slots, obj => {
                sessions.push({
                  id: o.id,
                  type: wellness.type,
                  calendarId: wellness.id,
                  category: 'time',
                  raw: {
                    ...o,
                    type: 'wellness',
                    time: obj.time
                  },
                  dueDateClass: '',
                  title: `Wellness - ${o.title} by ${o.host} at ${o.location}`,
                  start: moment(
                    `${obj.date} ${obj.time}${payload.timezone ==
                      'America/Los_Angeles' && '-07:00'}`,
                    'DD/MM/YYYY HH:mmZ'
                  ).format()
                })
              })
            }
          })

          let bookings = _.concat(roomBookings, reservations, sessions)

          return bookings
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
