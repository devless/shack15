export default {
  state: {
    rooms: [],
    room: undefined,
    openings: undefined,
    categories: undefined
  },
  mutations: {
    SET_ROOMS: (state, payload) => {
      state.rooms = payload
    },
    SET_ROOM_CATEGORIES: (state, categories) => {
      state.categories = categories
    },
    SET_ROOM: (state, room) => {
      state.room = room
    },
    ADD_ROOM: (state, payload) => {
      state.rooms.push(payload)
    },
    UPDATE_ROOM: (state, payload) => {
      state.rooms[payload.index] = payload.row
    },
    DELETE_ROOM: (state, index) => {
      state.rooms.splice(index, 1)
    },
    SET_OPENING_DAYS: (state, data) => {
      state.openings = data
    }
  },
  actions: {
    async fetchRooms({ commit }, workspace_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'rooms', {
            where: `core_workspace_id,${workspace_id}`
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_ROOMS', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Failed to Retrieve Rooms. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getRoomCategories({ commit, dispatch }) {
      try {
        return await dispatch('superApi', {
          table: 'core_room_categories',
          action: 'read',
          queries: []
        }).then(res => {
          if (res.status_code === 1000) {
            commit('SET_ROOM_CATEGORIES', res.payload)
            return res.payload
          }
          this.$toast.error('Error Retrieving Room Categories')
          return false
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getOpeningDays({ commit }, room_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'room_opening_days', {
            where: 'core_rooms_id,' + room_id
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_OPENING_DAYS', res.payload.results)
              return
            }
            this.$toast.error('Failed to Retrieve Room Openings. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addRoom({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .addData('core', 'rooms', payload)
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_ROOM', payload)
              // this.$router.go(-1)
              this.$toast.success('Room Added Successfully')
              return res.payload.entry_id
            }
            this.$toast.error('Adding New Room Failed. Try Again ...')
            return false
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateRoom({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .updateData('core', 'rooms', 'id', payload.row.id, payload.row)
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 619) {
              commit('UPDATE_ROOM', payload)
              this.$router.go(-1)
              this.$toast.success('Room Updated Successfully')
              return
            }
            this.$toast.error('Room Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async deleteRoom({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .deleteData('core', 'rooms', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 636) {
              commit('DELETE_ROOM', payload.index)
              this.$toast.success('Room Deleted Successfully')
              return
            }
            this.$toast.error('Room Delete Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getSingleRoom({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .queryData('core', 'rooms', {
            where: [
              `core_workspace_id,${payload.workspace_id}`,
              `id,${payload.id}`
            ]
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625 && res.payload.results.length > 0) {
              let resp = res.payload.results[0]
              resp.amenities = JSON.parse(resp.amenities)
              resp.images = JSON.parse(resp.images)
              return commit('SET_ROOM', {
                row: resp
              })
            }
          })
      } catch (e) {
        this.$toast.error('Error Retrieving Room. Try Again...')
        this.$sentry.captureException(e)
      }
    }
  }
}
