export default {
  state: {
    options: [],
    current_opt: {}
  },
  mutations: {
    SET_OPTIONS: (state, options) => {
      state.options = options
    },
    ADD_OPTION: (state, option) => {
      state.options.push(option)
    },
    UPDATE_OPTION: (state, payload) => {
      state.options[payload.index] = payload.row
    },
    REMOVE_OPTION: (state, index) => {
      state.options.splice(index, 1)
    },
    SET_CURRENT_OPT: (state, opt) => {
      state.current_opt = opt
    }
  },
  actions: {
    async getOptions({ commit }) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless.queryData('core', 'give_back_options').then(res => {
          commit('CHANGE_LOADING', false)
          if (res.status_code === 625) {
            commit('SET_OPTIONS', res.payload.results)
            return
          }
          this.$toast.error('Error Retrieving Give Back Options. Try Again...')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addOption({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .addData('core', 'give_back_options', payload)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_OPTION', payload)
              this.$router.go(-1)
              this.$toast.success('Give Back Option Added Successfully')
              return
            }
            this.$toast.error('Give Back Option Addition Failed')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateOption({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .updateData(
            'core',
            'give_back_options',
            'id',
            payload.row.id,
            payload.row
          )
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 619) {
              commit('UPDATE_OPTION', payload)
              this.$router.go(-1)
              this.$toast.success('Give Back Option Updated')
              return
            }
            this.$toast.error('Give Back Option Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async removeOption({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('core', 'give_back_options', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('REMOVE_OPTION', payload.index)
              this.$toast.success('Give Back Option Removed Successfully')
              return
            }
            this.$toast.error('Give Back Option Removal Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
