export default {
  state: {
    data: [],
    wellness: undefined,
    categories: []
  },
  mutations: {
    SET_DATA: (state, payload) => {
      state.data = payload
    },
    SET_WELLNESS: (state, wellness) => {
      state.wellness = wellness
    },
    ADD_WELLNESS: (state, payload) => {
      state.data.push(payload)
    },
    UPDATE_WELLNESS: (state, payload) => {
      state.data[payload.index] = payload.row
    },
    DELETE_WELLNESS: (state, index) => {
      state.data.splice(index, 1)
    },
    ADD_SLOT: (state, slot) => {
      state.wellness.slot.push(slot)
    },
    SET_WELLNESS_CATEGORIES: (state, categories) => {
      state.categories = categories
    }
  },
  actions: {
    async fetchData({ commit }, workspace_id) {
      try {
        commit('CHANGE_LOADING')
        return await this.$devless
          .queryData('core', 'wellness_sessions', {
            where: `core_workspace_id,${workspace_id}`
          })
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 625) {
              commit('SET_DATA', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Failed to Retrieve Wellness. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getSingleWellness({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'wellness_sessions', {
            where: payload
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625 && res.payload.results.length > 0) {
              let payload = {
                row: res.payload.results[0]
              }
              commit('SET_WELLNESS', payload)
              return res.payload.results[0]
            }
            this.$toast.error('Failed to Retrieve Wellness and Might Not Exist')
            this.$router.go(-1)
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addWellness({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .addData('core', 'wellness_sessions', payload)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 609) {
              payload.id = res.payload.results
              commit('ADD_WELLNESS', payload)
              this.$router.go(-1)
              this.$toast.success('Wellness Added Successfully')
              return
            }
            this.$toast.error('Wellness Addition Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateWellness({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .updateData('core', 'wellness_sessions', 'id', payload.id, payload)
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 619) {
              this.$router.go(-1)
              this.$toast.success('Wellness Updated Successfully')
              return
            }
            this.$toast.error('Wellness Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async deleteWellness({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('core', 'wellness_sessions', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('DELETE_WELLNESS', payload.index)
              this.$toast.success('Wellness Deleted Successfully')
              return
            }
            this.$toast.error('Wellness Delete Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getWellnessCategories({ commit }) {
      try {
        return await this.$devless
          .queryData('core', 'wellness_categories')
          .then(res => {
            if (res.status_code === 625) {
              commit('SET_WELLNESS_CATEGORIES', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Error Retrieving Wellness Categories')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
