const auth = {
  state: {
    authUser: null,
    loggedIn: false
  },
  mutations: {
    CHANGE_AUTH_STATE: (state, opt = false) => {
      if (opt) {
        state.loggedIn = opt
      } else {
        state.loggedIn = !state.loggedIn
      }
    },
    SET_USER: (state, user) => {
      state.authUser = user
    }
  },
  actions: {
    async login({ commit }, payload) {
      try {
        return await this.$devless
          .call('core', 'login', [payload])
          .then(res => {
            if (
              res.status_code === 1000 &&
              res.payload.profile.is_admin === 1
            ) {
              commit('SET_USER', res.payload)
              commit('CHANGE_AUTH_STATE', true)
              this.$auth.$storage.setUniversal(
                'token',
                res.payload.token,
                false
              )

              this.$router.push('/')

              this.$toast.success(res.message)
              return true
            }
            this.$toast.error('Invalid Credentials')
            return false
          })
      } catch (error) {
        this.$sentry.captureException(
          new Error(`${payload.email} : Bad credentials`)
        )
      }
    },
    async getProfile({ commit }, payload) {
      try {
        let token = this.$auth.$storage.getUniversal('token', false)
        this.$devless.setToken(token)
        return await this.$devless.call('core', 'getUserProfile').then(res => {
          if (res.status_code === 1000) {
            commit('SET_USER', {
              profile: res.payload,
              token: token
            })
            commit('CHANGE_AUTH_STATE')
            return true
          }
          return false
        })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async sendResetCode(context, payload) {
      try {
        return await this.$devless
          .call('core', 'sendPasswordResetCode', [payload])
          .then(res => {
            if (res.status_code === 1000) {
              this.$toast.success(
                'Check your email for the password reset link'
              )
              return true
            }
          })
      } catch (error) {
        this.$sentry.captureException(
          new Error(`${payload.email} : Unable to reset password`)
        )
      }
    },
    async resetPassword(context, payload) {
      try {
        return await this.$devless
          .call('core', 'resetPasswordUsingResetCode', [payload])
          .then(res => {
            if (res.status_code === 1000) {
              this.$toast.success(res.message)
              return true
            }
            this.$toast.error(res.message)
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async logout({ commit }) {
      let token = this.$auth.$storage.getUniversal('token', false)
      this.$devless.setToken(token)
      commit('CHANGE_LOADING', true)
      return await this.$devless.call('core', 'logout').then(async res => {
        commit('CHANGE_LOADING', false)
        if (res.status_code === 1000) {
          this.$auth.$storage.setUniversal('token', null, false)
          commit('CHANGE_AUTH_STATE', false)
        } else {
          commit('CHANGE_AUTH_STATE', false)
          this.$toast.error('Logging Out Failed.')
        }
      })
    }
  },
  getters: {}
}

export default auth
