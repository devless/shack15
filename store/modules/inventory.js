import _ from 'lodash'

export default {
  state: {
    data: [],
    inventory: {},
    orders: [],
    meal_times: [],
    meal_time: undefined
  },
  mutations: {
    SET_INVENTORY: (state, inventory) => {
      state.data = inventory
    },
    SET_ORDERS: (state, orders) => {
      state.orders = orders
    },
    ADD_INVENTORY: (state, inventory) => {
      state.data.push(inventory)
    },
    UPDATE_INVENTORY: (state, payload) => {
      state.data[payload.index] = payload.row
    },
    DELETE_INVENTORY: (state, index) => {
      state.data.splice(index, 1)
    },
    SET_CURRENT_INVENTORY: (state, inventory) => {
      state.inventory = inventory
    },
    SET_LABELS: (state, meal_times) => {
      state.meal_times = meal_times
    },
    SET_LABEL: (state, meal_time) => {
      state.meal_time = meal_time
    },
    ADD_LABELS: (state, meal_time) => {
      state.meal_times.push(meal_time)
    },
    REMOVE_LABEL: (state, index) => {
      state.meal_times.splice(index, 1)
    }
  },
  actions: {
    async getAllOrders({ commit }, payload) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .queryData('core', 'eatry_orders', {
            where: [`core_workspace_id,${payload.workspace_id}`],
            desc: 'id',
            related: '*'
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_ORDERS', res.payload.results)
              return
            }
            this.$toast.error('Error Fetching Orders. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getFoodOrders({ commit, dispatch, getters }, payload) {
      await dispatch('superApi', {
        table: 'core_eatry_orders',
        action: 'read',
        queries: [
          {
            join: ['users', 'core_eatry_orders.users_id', '=', 'users.id']
          },
          {
            join: [
              'core_eatry',
              'core_eatry_orders.core_eatry_id',
              '=',
              'core_eatry.id'
            ]
          },
          {
            where: [
              'core_eatry_orders.core_workspace_id',
              getters.getCurrentSpaceId
            ]
          },
          {
            where: ['core_eatry_orders.status', payload]
          },
          {
            orderBy: ['core_eatry_orders.id', 'desc']
          },
          {
            select: [
              'core_eatry_orders.id as c_id',
              'core_eatry_orders.*',
              'users.first_name',
              'users.last_name',
              'core_eatry.*'
            ]
          }
        ]
      }).then(res => {
        if (res.status_code == 1000) {
          commit('SET_ORDERS', res.payload)
        }
      })
    },
    async getLatestOrders({ commit }, payload) {
      try {
        // commit('CHANGE_LOADING', true)
        return await this.$devless
          .queryData('core', 'eatry_orders', {
            where: [`core_workspace_id,${payload.workspace_id}`],
            desc: 'id',
            related: '*'
          })
          .then(res => {
            // commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              // commit('SET_ORDERS', res.payload.results)
              return _.filter(res.payload.results, item => {
                return item.status == null
              })
            }
            this.$toast.error('Error Fetching Latest Orders. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getInventory({ commit }, option) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .queryData('core', 'eatry', {
            where: [
              `core_workspace_id,${option.workspace}`,
              `type,${option.type}`
            ]
          })
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_INVENTORY', res.payload.results)
              return
            }
            this.$toast.error('Error Fetching Inventory. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addInventory({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless.addData('core', 'eatry', payload).then(res => {
          commit('CHANGE_LOADING')
          if (res.status_code === 609) {
            payload.id = res.payload.entry_id
            commit('ADD_INVENTORY', payload)
            this.$router.go(-1)
            this.$toast.success('Inventory Added Successfully')
            return
          }
          this.$toast.error('Error Adding Inventory. Try Again ...')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateInventory({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .updateData('core', 'eatry', 'id', payload.row.id, payload.row)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 619) {
              commit('UPDATE_INVENTORY', payload)
              this.$router.go(-1)
              this.$toast.success('Inventory Updated Successfully')
              return
            }
            this.$toast.error('Error Updating Inventory. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async removeInvetory({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('core', 'eatry', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('DELETE_INVENTORY', payload.index)
              this.$toast.success('Inventory Removed Successfully')
              return
            }
            this.$toast.error('Error Deleting Inventory. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async fetchLabels({ commit }) {
      try {
        return await this.$devless
          .queryData('core', 'eatry_specials')
          .then(res => {
            if (res.status_code === 625) {
              commit('SET_LABELS', res.payload.results)
              return res.payload.results
            }
            this.$toast.error('Error Retrieving Meal Labels')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async addLabel({ commit }, payload) {
      try {
        return await this.$devless
          .addData('core', 'eatry_specials', payload)
          .then(res => {
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_LABELS', payload)
              this.$router.go(-1)
              this.$toast.success('Label Added Successfully')
              return res.payload.results
            }
            this.$toast.error('Error Adding New Meal Labels')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateLabel({ commit }, payload) {
      try {
        return await this.$devless
          .updateData('core', 'eatry_specials', 'id', payload.id, payload)
          .then(res => {
            if (res.status_code === 619) {
              this.$router.go(-1)
              this.$toast.success('Label Updated Successfully')
              return res
            }
            this.$toast.error('Error Updating Label. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async deleteLabel({ state, commit }, index) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .deleteData(
            'core',
            'eatry_specials',
            'id',
            state.meal_times[index].id
          )
          .then(res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 636) {
              commit('REMOVE_LABEL', index)
              this.$toast.success('Label Removed Successfully')
              return res
            }
            this.$toast.error('Error Deleting Label. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    }
  }
}
