const members = {
  state: {
    // eslint-disable-next-line prettier/prettier
    data: [],
    member: {},
    membership_packages: [],
    checkins: []
  },
  mutations: {
    SET_MEMBERS: (state, members) => {
      state.data = members
    },
    SET_MEMBER: (state, member) => {
      state.member = member
    },
    ADD_MEMBER: (state, member) => {
      state.data.push(member)
    },
    UPDATE_MEMBER: (state, payload) => {
      state.data[payload.index] = payload.row
    },
    SET_MEMBERSHIP_PACKAGES: (state, memberships) => {
      state.membership_packages = memberships
    },
    APPROVE_MEMBER: (state, index) => {
      state.data.splice(index, 1)
    },
    REJECT_MEMBER: (state, index) => {
      state.data.splice(index, 1)
    },
    ADD_CHECKINS: (state, member) => {
      state.checkins.push(member)
    }
  },
  actions: {
    async getAllMembers({ commit, dispatch }, workspace_id) {
      try {
        commit('CHANGE_LOADING', true)
        commit('SET_MEMBERS', [])
        return await this.$devless
          .call('core', 'getAllUsers', [{ workspace_id: workspace_id }])
          .then(async res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 1000) {
              commit(
                'SET_MEMBERS',
                _.sortBy(res.payload, o => {
                  return o.first_name
                })
              )
              return res.payload
            }
            if (
              res.message.includes('User is not authenticated') ||
              res.message.includes('you are not authorised')
            ) {
              await dispatch('logout')
              this.$toast.error('Error! Session Expired. Login to Continue ...')
              return
            }
            this.$toast.error(res.message)
            return
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async getUnapprovedMembers({ commit, dispatch }) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .call('core', 'listPendingAccounts')
          .then(async res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 1000) {
              commit('SET_MEMBERS', res.payload)
              return
            }

            if (
              res.message.includes('User is not authenticated') ||
              res.message.includes('you are not authorised')
            ) {
              await dispatch('logout')
              this.$toast.error('Error! Session Expired. Login to Continue ...')
              return
            }

            this.$toast.error(res.message)
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async getRejectedMembers({ commit, dispatch }) {
      try {
        commit('CHANGE_LOADING', true)
        await this.$devless
          .call('core', 'getDisapprovedAccounts')
          .then(async res => {
            commit('CHANGE_LOADING', false)
            if (res.status_code === 1000) {
              commit('SET_MEMBERS', res.payload)
              return
            }

            if (
              res.message.includes('User is not authenticated') ||
              res.message.includes('you are not authorised')
            ) {
              await dispatch('logout')
              this.$toast.error('Error! Session Expired. Login to Continue ...')
              return
            }

            this.$toast.error(res.message)
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async addMember({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .call('core', 'registerCustomer', [payload])
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 1000) {
              commit('ADD_MEMBER', payload)
              localStorage.removeItem('shack15-new-member')
              this.$router.go(-1)
              this.$toast.success('Member Successfully Added')
              return true
            }
            this.$toast.error(res.message)
            return false
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async getMembershipPackages({ commit }, workspace_id) {
      try {
        // commit('CHANGE_LOADING', true)
        await this.$devless
          .queryData('core', 'membership_packages', {
            where: `core_workspace_id,${workspace_id}`
          })
          .then(res => {
            // commit('CHANGE_LOADING', false)
            if (res.status_code === 625) {
              commit('SET_MEMBERSHIP_PACKAGES', res.payload.results)
              return
            }
            this.$toast.error(res.message)
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async approveMember({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        return await this.$devless
          .call('core', 'approveAccounts', [{ account_ids: [payload.row.id] }])
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 1000) {
              commit('APPROVE_MEMBER', payload.index)
              this.$toast.success('Membership Approved Successfully')
              return true
            }
            this.$toast.error('Membership Approval Failed. Try Again ...')
            return false
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async rejectMember({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        return await this.$devless
          .call('core', 'disapproveAccounts', [
            { account_ids: [payload.row.id] }
          ])
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 1000) {
              commit('REJECT_MEMBER', payload.index)
              this.$toast.success('Membership Rejected Successfully')
              return
            }
            this.$toast.error('Membership Rejection Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async updateMember({ state, commit, dispatch }, payload) {
      try {
        let id = payload.row.id
        delete payload.row.id

        commit('CHANGE_LOADING', true)
        return await dispatch('superApi', {
          table: 'users',
          action: 'update',
          queries: [{ where: ['users.id', state.member.row.id] }],
          records: payload.row
        }).then(res => {
          if (res.status_code === 1000) {
            payload.row.id = id
            commit('CHANGE_LOADING', false)
            commit('UPDATE_MEMBER', payload)
            this.$toast.success('Member Details Updated Successfully')
            return res
          }
          this.$toast.error('Member Details Update Failed. Try Again ...')
        })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getUserById({ commit }, user_id) {
      try {
        commit('CHANGE_LOADING', true)
        return await this.$devless
          .call('core', 'getAUserProfile', [
            {
              user_id: user_id
            }
          ])
          .then(res => {
            if (res.status_code === 1000) {
              commit('SET_MEMBER', { row: res.payload.results[0] })
              commit('CHANGE_LOADING', false)
              return res
            }
            this.$toast.error('Error Getting Specific User. Try Again ...')
            return false
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async getPendingAcceptance({ commit, dispatch }) {
      commit('CHANGE_LOADING', true)
      await dispatch('superApi', {
        table: 'users',
        action: 'read',
        queries: [{ where: ['invited', '!=', 1] }]
      }).then(res => {
        commit('CHANGE_LOADING', false)
        if (res.status_code === 1000) {
          commit('SET_MEMBERS', res.payload)
          return
        }
        this.$toast.error(res.message)
      })
    }
  },
  getters: {}
}

export default members
