export default {
  state: {
    plans: [],
    plan: undefined
  },
  mutations: {
    SET_DATA: (state, plans) => {
      state.plans = plans
    },
    ADD_PLAN: (state, plan) => {
      state.plans.push(plan)
    },
    UPDATE_PLAN: (state, payload) => {
      state.plans[payload.index] = payload.row
    },
    REMOVE_PLAN: (state, index) => {
      state.plans.splice(index, 1)
    },
    SET_CURRENT_PLAN: (state, plan) => {
      state.plan = plan
    }
  },
  actions: {
    async getPlans({ commit }, id) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .queryData('core', 'membership_packages', {
            where: `core_workspace_id,${id}`
          })
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 625) {
              commit('SET_DATA', res.payload.results)
              return
            }
            this.$toast.error('Error Retrieving Plans. Try Again ...')
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    },
    async addPlan({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .addData('core', 'membership_packages', payload)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 609) {
              payload.id = res.payload.entry_id
              commit('ADD_PLAN', payload)
              this.$router.go(-1)
              this.$toast.success('Plan Added Successfully')
              return
            }
            this.$toast.error('Error Adding Plan. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(error)
      }
    },
    async updatePlan({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .updateData(
            'core',
            'membership_packages',
            'id',
            payload.row.id,
            payload.row
          )
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 619) {
              this.commit('UPDATE_PLAN', payload)
              this.commit('SET_CURRENT_PLAN', payload)
              this.$router.go(-1)
              this.$toast.success('Plan Updated Successfully')
              return
            }
            this.$toast.error('Plan Update Failed. Try Again ...')
          })
      } catch (e) {
        this.$sentry.captureException(e)
      }
    },
    async removePlan({ commit }, payload) {
      try {
        commit('CHANGE_LOADING')
        await this.$devless
          .deleteData('core', 'membership_packages', 'id', payload.row.id)
          .then(res => {
            commit('CHANGE_LOADING')
            if (res.status_code === 636) {
              commit('REMOVE_PLAN', payload.index)
              if (payload.level !== 0) {
                this.$router.go(-1)
              }
              this.$toast.success('Plan Remove Successfully')
              return true
            }
            this.$toast.error('Plan Delete Failed. Try Again ...')
          })
      } catch (error) {
        this.$sentry.captureException(error)
      }
    }
  },
  getters: {}
}
