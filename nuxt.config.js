const pkg = require('./package')
const webpack = require('webpack')

module.exports = {
  mode: 'universal',
  /*
   ** Global auth middleware for router
   */
  router: {
    middleware: ['auth']
  },
  env: {
    devless: {
      proxy: true, // When set to `true` would use the proxy url for all requests(ssr) but `false` would use the absolute path for the DevLess instance
      host: 'https://api.shack15.com',
      'devless-token': 'abe383446da08c45ce332b4a1d947ef1'
    },
    stripe: {
      publishable_key: 'pk_test_EldDf62abr3QSC1ju7jhfdAs',
      secret_key: 'sk_test_On1uFFOK31r2vCBDSH1ofldL'
    }
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'SHACK15 CRM',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    bodyAttrs: {
      class: 'fixed-left'
    },
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: '/css/bootstrap.min.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/bootstrap-vue.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/core.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/components.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/icons.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/pages.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/menu.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/responsive.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/helpers.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/custom.css'
      }
    ],
    script: [
      {
        src: '/js/modernizr.min.js',
        body: false
      },
      {
        src: '/js/jquery.min.js',
        body: true
      },
      {
        src: '/js/bootstrap.min.js',
        body: true
      },
      {
        src: '/js/detect.js',
        body: true
      },
      {
        src: '/js/fastclick.js',
        body: true
      },
      {
        src: '/js/waves.js',
        body: true
      },
      {
        src: '/js/jquery.slimscroll.js',
        body: true
      },
      {
        src: 'https://js.stripe.com/v3/',
        body: true
      },
      {
        src: 'https://js.pusher.com/4.4/pusher.min.js'
      }
      // { src: '/js/jquery.app.js', body: true }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#000'
  },

  /*
   ** Global CSS
   */
  css: [
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/devless',
    '~/plugins/vue2-filters',
    {
      src: '~/plugins/vue-good-table',
      ssr: true
    },
    {
      src: '~/plugins/plugins',
      ssr: false
    },
    {
      src: '~/plugins/vue-fullcalendar',
      ssr: false
    },
    {
      src: '~/plugins/vue-content-placeholders',
      ssr: false
    },
    { src: '~/plugins/vue2-datepicker', ssr: false },
    { src: '~/plugins/vue-multiselect', ssr: false },
    { src: '~/plugins/vue-pusher', ssr: false },
    { src: '~/plugins/vue-quill-editor.js', ssr: false },
    { src: '~/plugins/tui-calendar', ssr: false },
    { src: '~/plugins/vue-storage', ssr: false }
  ],
  vendor: ['~/plugins/devless'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/sentry',
    '@nuxtjs/toast',
    '@nuxtjs/moment',
    '@nuxtjs/font-awesome',
    [
      'bootstrap-vue/nuxt',
      {
        css: false
      }
    ]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    proxy: true,
    https: true,
    retry: {
      retries: 3
    }
  },
  proxy: {
    '/api/v1/': {
      target: 'https://api.shack15.com',
      changeOrigin: true
    }
  },
  auth: {},
  sentry: {
    public_key: 'f511ffdd35a0487aa05c4bf3339af7e2',
    project_id: '1332065',
    config: {}
  },
  toast: {
    singleton: true,
    fullWidth: true,
    duration: 1000,
    iconPack: 'fontawesome',
    action: {
      icon: 'fa-times',
      onClick: (e, toastObject) => {
        toastObject.goAway(0)
      },
      class: 'close'
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        _: 'lodash'
      })
    ]
  }
}
