export const days = [
  {
    text: 'Monday',
    value: 'Monday'
  },
  {
    text: 'Tuesday',
    value: 'Tuesday'
  },
  {
    text: 'Wednesday',
    value: 'Wednesday'
  },
  {
    text: 'Thursday',
    value: 'Thursday'
  },
  {
    text: 'Friday',
    value: 'Friday'
  },
  {
    text: 'Saturday',
    value: 'Saturday'
  },
  {
    text: 'Sunday',
    value: 'Sunday'
  }
]
